#include <iostream>
#include <queue>

using namespace std;

int main()
{
    priority_queue <int,vector <int>, less<int>> kolejka1;
    priority_queue <int,vector <int>, greater<int>> kolejka2;
    int x=0;

    do{
        cout << "Wprowadź liczbę do kolejki (0 - kończy działanie: ";
        x=0;
        cin >> x;

        if (x!=0) ( kolejka1.push(x), kolejka2.push(x) );
        }

    while (x!=0);

    cout << "Liczby w kolejce posortowane malejąco: " << endl;
    while (kolejka1.empty()==false)
    {
        cout << kolejka1.top() << ", ";
        kolejka1.pop();
        }

    cout << '\n' << "Liczby w kolejce posortowane rosnąco: " << endl;
    while (kolejka2.empty()==false)
    {
        cout << kolejka2.top() << ", ";
        kolejka2.pop();
        }

    return 0;
}
