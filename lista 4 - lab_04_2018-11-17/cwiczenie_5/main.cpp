#include <iostream>
#include <queue>

using namespace std;

int x;

int main()
{
    priority_queue <int> kolejka;

    kolejka.push(5);
    kolejka.push(10);

    if ( kolejka.empty() ){
        cout << "Kolejka jest pusta";}

    else{
        cout << "Wprowadź liczbę: ";
        cin >> x;

        kolejka.push(x);

        cout << "Pierwszy element kolejki = " << kolejka.top() << endl;

        kolejka.push( kolejka.top()+100 );

        cout << "Pierwszy element w kolejce po dodaniu 100 = " << kolejka.top() << endl;
        cout << "Ilość liczb w kolejce = " << kolejka.size();
        }

    return 0;
}
