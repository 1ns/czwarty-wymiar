#include <iostream>
#include <stack>

using namespace std;

int main()
{
    stack < int > stosliczb;
    int liczba=0;

    do{
        cout << "Wprowadź liczbę na stos (0 - kończy): ";
        liczba=0;
        cin >> liczba;

        if (liczba!=0) stosliczb.push(liczba);

    } while (liczba!=0);

    cout << "Ilość liczb na stosie: " << stosliczb.size();

    return 0;
}
