#include <iostream>
#include <stack>

using namespace std;

int main()
{
    stack < int > stosliczb;
    int liczba=0;

    do{
        cout << "Wprowadź liczbę na stos (0 - kończy): ";
        liczba=0;
        cin >> liczba;

        if (liczba!=0) stosliczb.push(liczba);

    } while (liczba!=0);

    cout << "Liczby które zostały zdjęte ze stosu: ";

    if (stosliczb.empty()==true){
    cout<<"brak wprowadzonych liczb";
    }

    while (stosliczb.empty()==false){
    cout << stosliczb.top() << ", ";
    stosliczb.pop();
    }

    return 0;
}
