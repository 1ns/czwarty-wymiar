#include <iostream>
#include <cctype>
#include <cstring>
#include <unistd.h>

using namespace std;

const int n=100;

class slownik{
    public: slownik *t[n];
};

int do_indeksu(char c){
    if (c<='Z' && c>='A' || c<='z' && c>='a'){
            return toupper(c)-'A';
        }
    else{
            if(c==' ') return 26;
            if(c=='-') return 27;
        }
    return -1;
}

char z_indeksu(int n){
    if(n>=0 && n<=('z'-'a')){
            return ((char)n+'a');
        }
}

void zapisz(char *slowo,slownik *p){
    slownik *q;
    int pos;
    for(int i=1; i<=strlen(slowo); i++){
            pos=do_indeksu(slowo[i-1]);
            if(p->t[pos] != 0) p=p->t[pos];
        else{
                q=new slownik;
                p->t[pos]=q;
                for(int k=0; k<n; q->t[k++]=0);
                p=q;
            }
        }
    p->t[n-1]=p;
}

void pisz_slownik(slownik *p,char *bufor,char *ptr){
    for(int i=0;i<28;++i){
            if(p->t[i]){
                    *ptr=z_indeksu(i);
                    if(p->t[i]->t[n-1]){
                            *(ptr+1)=0;
                            cout << bufor << endl;
                        }
                    pisz_slownik(p->t[i],bufor,ptr+1);
                }
        }
}

void szukaj(char *slowo, slownik *p){
    int test=1;
    int i=0;
    while ((test==1) && i<strlen(slowo)){
        if(p->t[do_indeksu(slowo[i])]==0)
            test=0;
        else
            p=p->t[do_indeksu(slowo[i++])];
    }
    if(i==strlen(slowo) && p->t[n-1]==p && test){
        cout << "!!! Slowo znalezione!" << endl;
        cout << "Wybór: ";}
    else{
        cout << "!!! Slowo nie znalezione" << endl;
        cout << "Wybór: ";}
}

main(){
    int i;
    char tresc[100];
    slownik *p=new slownik;
    for (i=0; i<n; p->t[i++]=0);
    char c='x';
start:
    cout << "1 - Dodaj słowo" << endl;
    cout << "2 - Znajdź słowo" << endl;
    cout << "3 - Pokaż słownik" << endl;
    cout << "0 - Zamknij program" << endl;
    cout << "Wybór: ";
    while (c!='0'){
        cin >> c;
        switch (c){
        case '1':
            cout << "Dodaj: ";
            cin >> tresc;
            zapisz(tresc,p);
            system("clear");
            goto start;
            break;

        case '2':
            cout << "Szukaj: ";
            cin >> tresc;
            szukaj(tresc,p);
            break;

        case '3':
            system("clear");
            cout << "- - - - - - - - - - - SŁOWNIK - - - - - - - - - - -" << endl;
            pisz_slownik(p,tresc,tresc);
            cout << "- - - - - - - - - - - - - - - - - - - - - - - - - -" << endl;
            goto start;
            break;

        default:
            break;
        }
    }
}
