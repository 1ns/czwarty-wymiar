#include <iostream>
#include <string>

using namespace std;

struct BTNode
{
    BTNode * left;
    BTNode * right;
    int data;
};

int n;
BTNode * root;
string cr,cl,cp;

void readBT()
{
    BTNode ** vp;
    int i,d,l,r;

    cout << "coś nie tak :/" << endl;
    cout << "Ilość węzłów drzewa: ";
    cin >> n;

    vp = new BTNode * [n];

    for(i = 0; i < n; i++)
        vp[i] = new BTNode;

    for(i = 0; i < n; i++)
        {
            cout << "odczytujemy dane węzła i numery synów" << endl;
            cin >> d >> l >> r;

        vp[i]->data = d;

        if(l) vp[i]->left = vp[l];
        else  vp[i]->left = NULL;

        if(r) vp[i]->right = vp[r];
        else  vp[i]->right = NULL;
    }

    root = vp[0];

    delete [] vp;

}

void DFSRelease(BTNode * v)
{
    if(v)
    {
        DFSRelease(v->left);
        DFSRelease(v->right);
        delete v;
    }
}

void printBT(string sp, string sn, BTNode * v)
{
  string s;

  if(v)
  {
    s = sp;
    if(sn == cr) s[s.length() - 2] = ' ';
    printBT(s + cp, cr, v->right);

    s = s.substr(0,sp.length()-2);
    cout << s << sn << v->data << endl;

    s = sp;
    if(sn == cl) s[s.length() - 2] = ' ';
    printBT(s + cp, cl, v->left);
  }
}

int main()
{
    cr = cl = cp = "  ";
    cr[0] = 218; cr[1] = 196;
    cl[0] = 192; cl[1] = 196;
    cp[0] = 179;

    readBT();
    printBT("","",root);
    DFSRelease(root);

    return 0;
}
