#include <iostream>
#include <iomanip>

using namespace std;

int main()
{

    int n,m,i,j,v1,v2;
    char ** A;

    cout << "Ilość wierzchołków: ";
    cin >> n;
    cout << "Ilość krawędzi: ";
    cin >> m;

    A = new char * [n];

    for(i = 0; i < n; i++)
    A[i] = new char [n];

    for(i = 0; i < n; i++)
    for(j = 0; j < n; j++) A[i][j] = 0;

    for(i = 0; i < m; i++)
        {
            cout<<"Wprowadź wierzchołki krawędzi " << i << endl;
            cout<<"v1 v2: ";
            cin >> v1 >> v2;

            A[v1][v2] = 1;{}
        }

        cout << endl;
        cout << "   ";

    for(i = 0; i < n; i++) cout << setw(3) << i;
    cout << endl << endl;
    for(i = 0; i < n; i++)

        {
            cout << setw(3) << i;
            for(j = 0; j < n; j++) cout << setw(3) << (int) A[i][j];
            cout << endl;
        }

    for(i = 0; i < n; i++) delete [] A[i];
    delete [] A;

    cout << endl;

    return 0;
}
