#include <iostream>
#include <iomanip>

using namespace std;

struct slistEl
{
    slistEl * next;
    int v;
};

int main()
{
    int n,m,i,v1,v2;
    slistEl ** A;
    slistEl *p,*r;

    cout << "Ilość wierzchołków: ";
    cin >> n;
    cout << "Ilość krawędzi: ";
    cin >> m;

    A = new slistEl * [n];

    for(i = 0; i < n; i++) A[i] = NULL;

    for(i = 0; i < m; i++)
        {
            cout<<"Wprowadź wierzchołki krawędzi " << i << endl;
            cout<<"v1 v2: ";
            cin >> v1 >> v2;
            p = new slistEl;
            p->v = v2;
            p->next = A[v1];
            A[v1] = p;
        }

    cout << endl;

    for(i = 0; i < n; i++)
        {
            cout << "A[" << i << "] =";
            p = A[i];
            while(p)

            {
                cout << setw(3) << p->v;
                p = p->next;
            }
        cout << endl;
        }

    for(i = 0; i < n; i++)
        {
            p = A[i];
            while(p)
            {
                r = p;
                p = p->next;
                delete r;
            }
        }

    delete [] A;

    cout << endl;

    return 0;
}
