#include <iostream>

using namespace std;

struct slistEl
{
    slistEl * next;
    int v,w;
};

const int MAXINT = 2147483647;

int main()
{
    int i,j,m,n,v,u,w,x,y,sptr,*d,*p,*S;
    bool *QS;
    slistEl **graf;
    slistEl *pw,*rw;

    cout << "Węzeł startowy: ";
    cin >> v;
    cout << "Ilość wierzchołków: ";
    cin >> n;
    cout << "Ilość krawędzi: ";
    cin >> m;

    d = new int [n];
    p = new int [n];
    QS = new bool [n];
    graf = new slistEl * [n];
    S = new int [n];
    sptr = 0;

    for(i = 0; i < n; i++)
    {
        d[i] = MAXINT;
        p[i] = -1;
        QS[i] = false;
        graf[i] = NULL;
    }

    for(i = 0; i < m; i++)
    {
        cout << "Wprowadź wierzchołki krawędzi oraz wage: " << endl;
        cout << "krawędź" << i << "= v1 v2 w: ";
        cin >> x >> y >> w;
        pw = new slistEl;
        pw->v = y;
        pw->w = w;
        pw->next = graf[x];
        graf[x] = pw;
    }

    cout << endl;

    d[v] = 0;

    for(i = 0; i < n; i++)
    {
        for(j = 0; QS[j]; j++);
        for(u = j++; j < n; j++)
        if(!QS[j] && (d[j] < d[u])) u = j;

        QS[u] = true;

        for(pw = graf[u]; pw; pw = pw->next)
        if(!QS[pw->v] && (d[pw->v] > d[u] + pw->w))
            {
                d[pw->v] = d[u] + pw->w;
                p[pw->v] = u;
            }
    }

    for(i = 0; i < n; i++)
        {
            cout << i << ": ";
            for(j = i; j > -1; j = p[j]) S[sptr++] = j;
            while(sptr) cout << S[--sptr] << " ";
            cout << "$" << d[i] << endl;
        }

    delete [] d;
    delete [] p;
    delete [] QS;
    delete [] S;

    for(i = 0; i < n; i++)
        {
            pw = graf[i];
            while(pw)
            {
                rw = pw;
                pw = pw->next;
                delete rw;
            }
        }

    delete [] graf;

    return 0;
}
