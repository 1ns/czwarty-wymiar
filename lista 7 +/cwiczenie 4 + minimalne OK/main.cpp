#include <iostream>

using namespace std;

struct Edge
{
    int v1,v2,weight;
};

class Queue
{
    private:
        Edge * Heap;
        int hpos;
    public:
        Queue(int n);
        ~Queue();
        Edge front();
        void push(Edge e);
        void pop();
};

struct DSNode
{
    int up,rank;
};

class DSStruct
{
    private:
        DSNode * Z;
    public:
        DSStruct(int n);
        ~DSStruct();
        void MakeSet(int v);
        int FindSet(int v);
        void UnionSets(Edge e);
};

struct TNode
{
    TNode * next;
    int v,weight;
};

class MSTree
{
    private:
        TNode ** A;
        int Alen;
        int weight;
    public:
        MSTree(int n);
        ~MSTree();
        void addEdge(Edge e);
        void print();
};

Queue::Queue(int n)
{
    Heap = new Edge [n];
    hpos = 0;
}

Queue::~Queue()
{
    delete [] Heap;
}

Edge Queue::front()
{
    return Heap[0];
}

void Queue::push(Edge e)
{
    int i,j;

    i = hpos++;
    j = (i - 1) >> 1;

    while(i && (Heap[j].weight > e.weight))
    {
        Heap[i] = Heap[j];
        i = j;
        j = (i - 1) >> 1;
    }

    Heap[i] = e;
}

void Queue::pop()
{
    int i,j;
    Edge e;

    if(hpos)
    {
        e = Heap[--hpos];

        i = 0;
        j = 1;

        while(j < hpos)
    {
        if((j + 1 < hpos) && (Heap[j + 1].weight < Heap[j].weight)) j++;
        if(e.weight <= Heap[j].weight) break;
        Heap[i] = Heap[j];
        i = j;
        j = (j << 1) + 1;
    }

    Heap[i] = e;
  }
}

DSStruct::DSStruct(int n)
{
    Z = new DSNode [n];
}

DSStruct::~DSStruct()
{
  delete [] Z;
}

void DSStruct::MakeSet(int v)
{
    Z[v].up   = v;
    Z[v].rank = 0;
}

int DSStruct::FindSet(int v)
{
    if(Z[v].up != v) Z[v].up = FindSet(Z[v].up);
    return Z[v].up;
}

void DSStruct::UnionSets(Edge e)
{
    int ru,rv;

    ru = FindSet(e.v1);
    rv = FindSet(e.v2);
    if(ru != rv)
    {
        if(Z[ru].rank > Z[rv].rank)
        Z[rv].up = ru;
        else
        {
            Z[ru].up = rv;
            if(Z[ru].rank == Z[rv].rank) Z[rv].rank++;
        }
    }
}

MSTree::MSTree(int n)
{
    int i;

    A = new TNode * [n];
    for(i = 0; i < n; i++) A[i] = NULL;
    Alen = n - 1;
    weight = 0;
}

MSTree::~MSTree()
{
    int i;
    TNode *p,*r;

    for(i = 0; i <= Alen; i++)
    {
        p = A[i];
        while(p)
        {
            r = p;
            p = p->next;
            delete r;
        }
    }

  delete [] A;
}

void MSTree::addEdge(Edge e)
{
    TNode *p;

    weight += e.weight;
    p = new TNode;
    p->v = e.v2;
    p->weight = e.weight;
    p->next = A[e.v1];
    A[e.v1] = p;

    p = new TNode;
    p->v = e.v1;
    p->weight = e.weight;
    p->next = A[e.v2];
    A[e.v2] = p;
}

void MSTree::print()
{
    int i;
    TNode *p;

    cout << endl;
    for(i = 0; i <= Alen; i++)
    {
        cout << "Wierzchołek " << i << " - ";
        for(p = A[i]; p; p = p->next) cout << p->v << ":" << p->weight << " ";
        cout << endl;
    }
    cout << endl << endl << "Minimalne drzewo rozpinające = " << weight << endl;
}

int main()
{
    int n,m;
    Edge e;
    int i;

    cout << "Ilość wierzchołków: ";
    cin >> n;
    cout << "Ilość krawędzi: ";
    cin >> m;

    DSStruct Z(n);
    Queue Q(m);
    MSTree T(n);

    for(i = 0; i < n; i++)
    Z.MakeSet(i);

    for(i = 0; i < m; i++)
    {
        cout << "e.v1 e.v2 e.weight: ";
        cin >> e.v1 >> e.v2 >> e.weight;
        Q.push(e);
    }

    for(i = 1; i < n; i++)
        {
        do
            {
            e = Q.front();
            Q.pop();
            } while(Z.FindSet(e.v1) == Z.FindSet(e.v2));
            T.addEdge(e);
            Z.UnionSets(e);
        }

    T.print();

    return 0;
}
