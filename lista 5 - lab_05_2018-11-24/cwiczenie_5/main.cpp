#include <iostream>
#include <list>

using namespace std;

int wybor, liczba;

int main()
{
    list <int> lista;

    lista.push_front(10);
    lista.push_front(345);
    lista.push_front(69);

poczatek:
    if (lista.empty()==true) {
        cout << "Zawartość listy jest pusta :/" << endl;
        cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - -";
        }

    else {
        cout << "Zawartość listy: ";
            for(list<int>::iterator it = lista.begin(); it != lista.end(); it++){
                cout << *it << " ";}
        cout << '\n' << "- - - - - - - - - - - - - - - - - - - - - - - - - - -";
            }

    cout << '\n' << '\n' << "M E N U: Wybierz działanie na elementach listy: " << endl;
    cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - -" << endl;
    //cout << "Wybierz działanie na elementach listy: " << endl;
    cout << "1. push_front - wprowadź element na początek listy" << endl;
    cout << "2. push_back - wprowadź element na końcu listy" << endl;
    cout << "3. pop_front - usuń element z początku listy" << endl;
    cout << "4. pop_back - usuń element z końca listy" << endl;
    cout << "5. size - zwraca ilość elementów na liście" << endl;
    cout << "6. max_size - zwraca maksymalną ilość elementów jaką może pomieścić lista" << endl;
    cout << "7. empty - sprawdź czy lista jest pusta" << endl;
    cout << "8. remove - usówa z listy wszystkie elementy o podanej wartości" << endl;
    cout << "9. sort - sortuje elementy na liście rosnąco" << endl;
    cout << "10. reverse - odwraca kolejność elementów na liście" << endl;
    cout << "0 - zakończ program" << endl;
    cout << "Wybór : ";
    cin >> wybor;

    switch(wybor){

    case 1:{
        system("clear");
        do {
            cout << "Wprowadź element na początek listy (0 - kończy): ";
            cin >> liczba;
            if (liczba!=0) lista.push_front(liczba);
            }
        while (liczba!=0);
        system("clear");
        goto poczatek;}
    break;

    case 2:{
        system("clear");
        do {
            cout << "Wprowadź element na końcu listy (0 - kończy): ";
            cin >> liczba;
            if (liczba!=0) lista.push_back(liczba);
            }
        while (liczba!=0);
        system("clear");
        goto poczatek;}
    break;

    case 3:{
        system("clear");
        if (lista.empty()==false){
        cout << "Usunięty element z początku listy: " << lista.front() << endl;
        lista.pop_front();
        goto poczatek;}
        else {
        system("clear");
        //cout <<"Brak elementów na liście" << endl;
        goto poczatek;}

        }
    break;

    case 4:{
        system("clear");
        if (lista.empty()==false){
        cout << "Usunięty element z końca listy: " << lista.back() << endl;
        lista.pop_back();
        goto poczatek;}
        else {
        system("clear");
        //cout <<"Brak elementów na liście" << endl;
        goto poczatek;}

        }
    break;

    case 5:{
        system("clear");
        cout << "Ilość elementów na liście: " << lista.size() << endl;
        goto poczatek;}
    break;

    case 6:{
        system("clear");
        cout << "Maksymalna ilość elementów jaką może pomieścić lista: " << lista.max_size() << endl;
        goto poczatek;}
    break;

    case 7:{
        system("clear");
        if (lista.empty()==false) cout << "Lista NIE jest pusta." << endl;
        else { cout << "UPS.. ";}
        goto poczatek;}
    break;

    case 8:{
        if (lista.empty()==false){
            cout << "Który element usunąć z listy: ";
            cin >> liczba;
            lista.remove(liczba);
            system("clear");
            goto poczatek;
            }

        else {
            system("clear");
            cout << "UPS.. "; // << goto ????
            }
        goto poczatek;
        }
    break;

    case 9:{
        if (lista.empty()==false){
            system("clear");
            cout << "Lista elementów posortowana rosnąco: ";
            lista.sort();
            for (list <int> ::iterator it = lista.begin(); it != lista.end(); it++){
                cout << *it << " ";}
                cout << '\n';
            goto poczatek;
        }
        else {
            system("clear");
            cout << "UPS.. ";
            }
        goto poczatek;}
    break;

    case 10:{
        if (lista.empty()==false){
            system("clear");
            cout << "Odwrócona kolejność elementów na liście: ";
            lista.reverse();
            for (list <int> ::iterator it = lista.begin(); it != lista.end(); it++){
                cout << *it << " ";}
                cout << '\n';
            goto poczatek;
        }
        else {
            system("clear");
            cout << "UPS.. ";
            }
        goto poczatek;}
    break;

    case 0:{return 0;}

    }
    return 0;
}
