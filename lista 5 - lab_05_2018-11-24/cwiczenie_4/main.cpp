#include <iostream>
#include <list>

using namespace std;

int x;

int main()
{
    list <int> lista;

    lista.push_front(35);
    lista.push_front(89);
    lista.push_back(-9);

    cout << "Lista wprowadzonych elementów: " << endl;
    for (list <int> ::iterator it = lista.begin(); it != lista.end(); it++)
        {
            cout << *it << " ";
        }

    cout << '\n' << "Element do wprowadzenia na początek listy - push_front(): ";
    cin >> x;
    lista.push_front(x);

    cout << "Element do wprowadzenia na koniec listy - push_back(): ";
    cin >> x;
    lista.push_back(x);


    cout << "Aktualna lista wprowadzonych elementów: " << endl;
    for (list <int> ::iterator it = lista.begin(); it != lista.end(); it++)
        {
            cout << *it << " ";
        }

    cout << '\n' << "Lista elementów posortowana rosnąco - sort(): " << endl;
    lista.sort();
    for (list <int> ::iterator it = lista.begin(); it != lista.end(); it++)
        {
            cout << *it << " ";
        }

    cout << '\n' << "Lista elementów posortowana malejąco - reverse(): " << endl;
    lista.reverse();
    for (list <int> ::iterator it = lista.begin(); it != lista.end(); it++)
        {
            cout << *it << " ";
        }

    return 0;
}
