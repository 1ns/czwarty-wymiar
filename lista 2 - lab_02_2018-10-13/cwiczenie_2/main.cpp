#include <iostream>

using namespace std;

float a,b,c,x,y;

int main()
{
    cout << "Sprawdzę, czy trójkat jest prostokątny." << endl;
    cout << "Podaj długość pierwszej przyprostokątnej a: ";
    cin >> a;
    cout << "Podaj długość drugiej przyprostokątnej b: ";
    cin >> b;
    cout << "Podaj długość przeciwprostokątnej c: ";
    cin >> c;

    x=a*a+b*b ;
    y=c*c;

    if (x==y){
        cout << "Trójkąt jest prostokątny.";
    }

    else{
        cout << "Trójkąt nie jest prostokątny.";
    }

    return 0;
}
