#include <iostream>

using namespace std;

float a,b,c,x;

int main()
{
    cout << "Obliczę wartość x z wyrażenia: 2ax+3b=c" << endl;

powrot:
    cout << "Wprowadź a: ";
    cin >> a;

    if (a==0){
        cout << "Wpowadzono cyfrę 0 więc x=0. Wprowadź liczbę różną od 0: " << endl;
        goto powrot;}

    cout << "Wprowadź b: ";
    cin >> b;
    cout << "Wprowadź c: ";
    cin >> c;

    x = (c-3*b)/(2*a);

    cout << "Wynik x = " << x;

    return 0;
}
