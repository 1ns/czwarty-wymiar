#include <iostream>

using namespace std;

void sortowanie (int tab[], int x)
{
    for (int i=0; i<x; i++)
    for (int j=1; j<x-i; j++)
    if (tab[j-1] > tab[j])
    swap (tab[j-1], tab[j]);
}

int main()
{
    int *tab, x=10;
    cout << "Wprowadż 10 liczb do posortowania." << endl;
    tab = new int [x];

    for (int i=0; i<x; i++){
        cout << "Liczba " << i+1 << " : ";
        cin >> tab[i];
    }

    cout << "Liczby przed sortowaniem: " << endl;
    for (int i=0; i<x; i++)
        cout << tab[i] << " ";

    sortowanie (tab,x);

    cout << '\n' << "Liczby po sortowaniu:" << endl;
    for (int i=0; i<x; i++)
        cout << tab[i] << " ";

    return 0;
}
