#include <iostream>

using namespace std;

void dec_to_bin(int liczba)
{
    if(liczba>0)
    {
		dec_to_bin (liczba/2);
		cout << liczba%2;
    }
}

int main()
{
	int liczba;

	cout << "Podaj liczbe w systemie DEC: ";
	cin >> liczba;
	cout << "Liczba DEC = "<< liczba <<" po zmianie na NKD = ";

	dec_to_bin (liczba);

	return 0;
}
