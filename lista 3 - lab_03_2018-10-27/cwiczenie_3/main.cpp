#include <iostream>

using namespace std;

void fib (int n)
{
    long long a=0, b=1;
    for (int i=0; i<n; i++){
        cout << i+1 <<" wyraz ciągu Fibonacciego: " << b << endl;
        b+=a;
        a=b-a;
        }
}

int main()
{
    int n;
    cout << "Podaj, ile chcesz wypisać wyrazów ciągu Fibonacciego: ";
    cin >> n;

    //cout << "0 wyraz ciągu Fibonacciego: 0" << endl;

    fib(n);

    return 0;
}
