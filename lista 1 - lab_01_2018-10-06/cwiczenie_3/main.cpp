#include <iostream>

using namespace std;

int liczba;

int main()
{
    cout << "Sprawdzę, czy wprowadzona liczba jest dodatnia czy ujemna." << endl;
    cout << "Wprowadź liczbę: ";
    cin >> liczba;

    if (liczba>0){
        cout << "Wprowadzona liczba jest dodatnia.";
    }

    else{
        cout << "Wprowadzona liczba jest ujemna.";
    }

    return 0;
}
