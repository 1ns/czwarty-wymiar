#include <iostream>

using namespace std;

string imie, kierunek;
int wiek;

int main()
{
    cout << "Wpisz imię: ";
    cin  >> imie;
    cout << "Ile masz lat: ";
    cin >> wiek;
    cout << "Podaj kierunek studiów: ";
    cin >> kierunek;

    cout << imie << ", lat " << wiek << ". Witamy na kierunku " << kierunek << ".";

    return 0;
}
