#include <iostream>
#include <math.h>
#include <unistd.h>
//#include <windows.h>

using namespace std;

float a, b, r, h, n;
int wybor;

long long silnia (int n){
    if(n<2) return 1;
    return n*silnia(n-1);
    }

int main()
{
poczatek:

    cout << "Wybierz działanie do obliczenia:" << endl;
    cout << "1 - pole kwadratu" << endl;
    cout << "2 - pole prostokąta" << endl;
    cout << "3 - pole koła" << endl;
    cout << "4 - objetość kuli" << endl;
    cout << "5 - objetość stożka" << endl;
    cout << "6 - objetość walca" << endl;
    cout << "7 - silnia" << endl;
    cout << "0 - wyjscie" << endl;

    cin >> wybor;

    switch(wybor){

    case 1:{
        cout << "Podaj długość boku kwadratu [cm]: ";
        cin >> a;
        cout << "Pole kwadratu = " << a*a << " cm2";
    }
    break;

    case 2:{
        cout << "Podaj długość pierwszego boku prostokąta [cm]: ";
        cin >> a;
        cout << "Podaj długość drugiego boku prostokąta [cm]: ";
        cin >> b;
        cout << "Pole prostokąta = " << a*b << " cm2";
    }
    break;

    case 3:{
        cout << "Podaj promień koła [cm]: ";
        cin >> r;
        cout << "Pole koła = " << (r*r)*M_PI << " cm2";
    }
    break;

    case 4:{
        cout << "Podaj promień kuli [cm]: ";
        cin >> r;
        cout << "Objętość kuli = " << (4*M_PI*(r*r*r))/3 << " cm3";
    }
    break;

    case 5:{
        cout << "Podaj promień podstawy stożka [cm]: ";
        cin >> r;
        cout << "Podaj wysokość stożka [cm]: ";
        cin >> h;
        cout << "Objętość stożka = " << (M_PI*(r*r)*h)/3 << " cm3";
    }
    break;

    case 6:{
        cout << "Podaj promień podstawy walca [cm]: ";
        cin >> r;
        cout << "Podaj wysokość walca [cm]: ";
        cin >> h;
        cout << "Objętość walca = " << M_PI*(r*r)*h << " cm3";
    }
    break;

    case 7:{
        cout << "Podaj liczbę z której policzę silnię: ";
        cin >> n;
        cout << "Wynik "<< n << "! = " << silnia(n);
    }
    break;

    case 0:{
        return 0;
    }
    break;
}

    cout << '\n' << "Wciśnij dowolny klawisz by powrócić do menu wyboru.";
    cin.ignore();
    cin.get();
    system("clear");
    //system("cls")
    goto poczatek;
    return 0;
}
